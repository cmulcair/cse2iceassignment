import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { allResolved } from 'q';
import { AstMemoryEfficientTransformer } from '@angular/compiler';

interface FootyTeams {
  logo: string;
  id: string;
  name: string;
  abbrev: string;
}

interface Sources {
  url: string;
  name: string;
  id: string;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'httptutorial';

  data: any;
  teamname: any;
  sources: any;
  tips: any;
  fourthId: String;
  selection: number;
  sourceArray: String[];
  userStory: number;
  allTips: number;
  noSources: number;
  winPrediction: number;

  //userStory 2
  games: any;
  games3: any;
  allResults = [];
  allResultsString;

  //userStory 3
  nextGames: any;
  allNextGames = [];
  opponent: String;

  //userStory4
  hosted: any;
  allHosted = [];

  //userStory4
  rival: any;
  allRival = [];

  //userStory5
  teamOpp: any;

  selectedLevel;
  favouriteTeam = "Carlton";

  constructor(private http: HttpClient) {

  }



  teamdata: Array<Object> = [
    { id: 0, name: "Adelaide" },
    { id: 1, name: "Brisbane Lions" },
    { id: 2, name: "Carlton" },
    { id: 3, name: "Collingwood" },
    { id: 4, name: "Essendon" },
    { id: 5, name: "Fremantle" },
    { id: 6, name: "Geelong" },
    { id: 7, name: "Gold Coast" },
    { id: 8, name: "Greater Western Sydney" },
    { id: 9, name: "Hawthorn" },
    { id: 10, name: "Melbourne" },
    { id: 11, name: "North Melbourne" },
    { id: 12, name: "Port Adelaide" },
    { id: 13, name: "Richmond" },
    { id: 14, name: "St Kilda" },
    { id: 15, name: "Sydney" },
    { id: 16, name: "West Coast" },
    { id: 17, name: "Western Bulldogs" }
  ];

  selected() {
    console.log("Selected: " + this.selectedLevel);
    //console.log(String(this.selectedLevel.name));
    this.favouriteTeam = this.selectedLevel.name;
    console.log(this.favouriteTeam);
    //this.userStory = 2;
    //this.Story2(this.selectedLevel.name);
  }

  Story1(fav) {
    this.http.get('https://api.squiggle.com.au/?q=tips;year=2019;round=10').subscribe(res3 => {


      this.userStory = 1;
      this.tips = res3;
      this.allTips = 0;
      this.noSources = 0;




      for (var i = 0; i < this.tips.tips.length; i++) {
        //if (this.tips.tips[i].tip === "Carlton" &&
        if (this.tips.tips[i].tip === fav &&
          this.tips.tips[i].year === 2019 &&
          this.tips.tips[i].round === 10
        ) {
          this.selection = +this.tips.tips[i].confidence;
          this.noSources = this.noSources + 1;
          this.allTips = this.allTips + this.selection;

        }
      }

      if (this.allTips > 0) {
        this.winPrediction = this.allTips / this.noSources;
      } else {
        this.winPrediction = 0;
      }


    });
  }


  Story2(fav) {
    this.http.get('https://api.squiggle.com.au/?q=games;year=2019').subscribe(res2 => {


      this.userStory = 2;
      this.games = res2;
      console.log("here");

      console.log(fav);
      this.allResults = [];

      for (var i = 0; i < this.games.games.length; i++) {
        // if (this.games.games[i].hteam === "Carlton" || this.games.games[i].ateam === "Carlton") {
        console.log("------------------------------------");
        console.log("Favourite: " + this.favouriteTeam);
        console.log("-------------------------------------");
        if (this.games.games[i].hteam === fav || this.games.games[i].ateam === fav) {
          if (this.games.games[i].complete === 100) {
            console.log(this.games.games[i].winner);

            console.log(this.games.games[i].hteam);
            console.log(this.games.games[i].hgoals);
            console.log(this.games.games[i].hbehinds);
            console.log(this.games.games[i].hscore);

            console.log(this.games.games[i].ateam);
            console.log(this.games.games[i].agoals);
            console.log(this.games.games[i].abehinds);
            console.log(this.games.games[i].ascore);



            this.allResults.push(this.games.games[i].round);
            this.allResults.push(this.games.games[i].winner);
            this.allResults.push(this.games.games[i].hteam);
            this.allResults.push(this.games.games[i].hgoals);
            this.allResults.push(this.games.games[i].hbehinds);
            this.allResults.push(this.games.games[i].hscore);

            this.allResults.push(this.games.games[i].ateam);
            this.allResults.push(this.games.games[i].agoals);
            this.allResults.push(this.games.games[i].abehinds);
            this.allResults.push(this.games.games[i].ascore);



          }

        }
      }
      console.log(this.allResults);
      this.allResultsString = JSON.stringify(this.allResults);
      console.log(this.allResultsString);


    });
  }

  Story3(fav) {
    this.http.get('https://api.squiggle.com.au/?q=games;year=2019').subscribe(res4 => {


      this.userStory = 3;
      this.nextGames = res4;
      //this.teamname = this.data.teams[1].name;
      this.games3 = res4;

      this.allNextGames = [];

      for (var i = 0; i < this.nextGames.games.length; i++) {
        //if (this.games.games[i].hteam === "Carlton" || this.games.games[i].ateam === "Carlton") {
        if (this.games3.games[i].hteam === fav || this.games3.games[i].ateam === fav) {
          if (this.games3.games[i].complete === 0) {
            this.allNextGames.push(this.games3.games[i].round);
            this.allNextGames.push(this.games3.games[i].venue);
            this.allNextGames.push(this.games3.games[i].date);
            //if (this.games.games[i].hteam === 'Carlton') {
            if (this.games3.games[i].hteam === fav) {
              this.opponent = this.games3.games[i].ateam;
            } else {
              this.opponent = this.games3.games[i].hteam;
            }
            this.allNextGames.push(this.opponent);
          }
        }
      }
      console.log(this.nextGames);

    });
  }

  Story4(fav) {
    this.http.get('https://api.squiggle.com.au/?q=games;year=2019').subscribe(res5 => {

      this.userStory = 4;
      this.hosted = res5;

      this.allHosted = [];

      for (var i = 0; i < this.hosted.games.length; i++) {
        //if (this.hosted.games[i].hteam === "Carlton") {
        if (this.hosted.games[i].hteam === fav) {

          if (this.hosted.games[i].complete === 100) {
            console.log(this.hosted.games[i].winner);

            console.log(this.hosted.games[i].hteam);
            console.log(this.hosted.games[i].hgoals);
            console.log(this.hosted.games[i].hbehinds);
            console.log(this.hosted.games[i].hscore);

            console.log(this.hosted.games[i].ateam);
            console.log(this.hosted.games[i].agoals);
            console.log(this.hosted.games[i].abehinds);
            console.log(this.hosted.games[i].ascore);



            this.allHosted.push(this.hosted.games[i].round);
            this.allHosted.push(this.hosted.games[i].winner);
            this.allHosted.push(this.hosted.games[i].hteam);
            this.allHosted.push(this.hosted.games[i].hgoals);
            this.allHosted.push(this.hosted.games[i].hbehinds);
            this.allHosted.push(this.hosted.games[i].hscore);

            this.allHosted.push(this.hosted.games[i].ateam);
            this.allHosted.push(this.hosted.games[i].agoals);
            this.allHosted.push(this.hosted.games[i].abehinds);
            this.allHosted.push(this.hosted.games[i].ascore);



          }

        }
      }
      console.log(this.allHosted);
      //this.allResultsString = JSON.stringify(this.allResults);
      //console.log(this.allResultsString);




    });
  }

  Story5(fav) {
    this.http.get('https://api.squiggle.com.au/?q=games;year=2019').subscribe(res6 => {

      this.userStory = 5;
      this.rival = res6;

      if (fav === "Carlton"){
        this.teamOpp = "Collingwood";
      }
      if (fav === "Adelaide"){
        this.teamOpp = "Port Adelaide";
      }
      if (fav === "Essendon"){
        this.teamOpp = "Hawthorn";
      }
      if (fav === "Fremantle"){
        this.teamOpp = "West Coast";
      }
      if (fav === "West Coast"){
        this.teamOpp = "Fremantle";
      }
      if (fav === "Brisbane Lions"){
        this.teamOpp = "Gold Coast";
      }
      if (fav === "Gold Coast"){
        this.teamOpp = "Brisbane Lions";
      }
      if (fav === "Geelong"){
        this.teamOpp = "Melbourne";
      }
      if (fav === "Collingwood"){
        this.teamOpp = "Carlton";
      }
      if (fav === "Greater Western Sydney"){
        this.teamOpp = "Sydney";
      }
      if (fav === "Hawthorn"){
        this.teamOpp = "Essendon";
      }
      if (fav === "Melbourne"){
        this.teamOpp = "Geelong";
      }
      if (fav === "Geelong"){
        this.teamOpp = "Melbourne";
      }
      if (fav === "North Melbourne"){
        this.teamOpp = "Richmond";
      }
      if (fav === "Richmond"){
        this.teamOpp = "North Melbourne";
      }
      if (fav === "Port Adelaide"){
        this.teamOpp = "Adelaide";
      }
      if (fav === "Geelong"){
        this.teamOpp = "Melbourne";
      }
      if (fav === "St Kilda"){
        this.teamOpp = "Western Bulldogs";
      }
      if (fav === "Western Bulldogs"){
        this.teamOpp = "St Kilda";
      }
      


      this.allRival = [];



      for (var i = 0; i < this.rival.games.length; i++) {
        //if (this.rival.games[i].hteam === "Carlton" || this.rival.games[i].ateam === "Carlton") {
          if (this.rival.games[i].hteam === fav || this.rival.games[i].ateam === fav) {
          if (this.rival.games[i].complete === 100) {
            //if (this.rival.games[i].hteam === "Richmond" || this.rival.games[i].ateam === "Richmond") {
            if (this.rival.games[i].hteam === this.teamOpp || this.rival.games[i].ateam === this.teamOpp) {
              console.log(this.rival.games[i].winner);

              console.log(this.rival.games[i].hteam);
              console.log(this.rival.games[i].hgoals);
              console.log(this.rival.games[i].hbehinds);
              console.log(this.rival.games[i].hscore);

              console.log(this.rival.games[i].ateam);
              console.log(this.rival.games[i].agoals);
              console.log(this.rival.games[i].abehinds);
              console.log(this.rival.games[i].ascore);



              this.allRival.push(this.rival.games[i].round);
              this.allRival.push(this.rival.games[i].winner);
              this.allRival.push(this.rival.games[i].hteam);
              this.allRival.push(this.rival.games[i].hgoals);
              this.allRival.push(this.rival.games[i].hbehinds);
              this.allRival.push(this.rival.games[i].hscore);

              this.allRival.push(this.rival.games[i].ateam);
              this.allRival.push(this.rival.games[i].agoals);
              this.allRival.push(this.rival.games[i].abehinds);
              this.allRival.push(this.rival.games[i].ascore);
            }


          }

        }
      }
      console.log(this.allRival);

    });



  }



  ngOnInit(): void {








  }

  myFunc() {
    this.userStory = 1;
    this.Story1(this.selectedLevel.name);
    console.log("function 1 called");
  }

  myFunc2() {
    this.userStory = 2;
    this.Story2(this.selectedLevel.name);
    console.log("function 2 called");

  }

  myFunc3() {
    this.userStory = 3;
    this.Story3(this.selectedLevel.name);
    console.log("function 3 called");
  }

  myFunc4() {
    this.userStory = 4;
    this.Story4(this.selectedLevel.name);
    console.log("function 4 called");
  }

  myFunc5() {
    this.userStory = 5;
    this.Story5(this.selectedLevel.name);
    console.log("function 5 called");
  }

  myFunc8() {
    this.userStory = 4;
    this.Story4(this.selectedLevel.name);
    console.log("function 8 called");
  }



}
